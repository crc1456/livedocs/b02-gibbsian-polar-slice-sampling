# Gibbsian Polar Slice Sampling

## LiveDocs
### Private Binderhub Server
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-Jupyterlab-orange)](http://livedocs-private.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fb02-gibbsian-polar-slice-sampling/HEAD)
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-JupyterClassic-orange)](http://livedocs-private.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fb02-gibbsian-polar-slice-sampling/HEAD?urlpath=tree)
[![Voila](https://img.shields.io/badge/CRC1456%20Binderhub-Voila-green)](http://livedocs-private.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fb02-gibbsian-polar-slice-sampling/HEAD?urlpath=voila)


### Public Binderhub Server
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-Jupyterlab-orange)](http://livedocs.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fb02-gibbsian-polar-slice-sampling/HEAD)
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-JupyterClassic-orange)](http://livedocs.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fb02-gibbsian-polar-slice-sampling/HEAD?urlpath=tree)
[![Voila](https://img.shields.io/badge/CRC1456%20Binderhub-Voila-green)](http://livedocs.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fb02-gibbsian-polar-slice-sampling/HEAD?urlpath=voila)

### Other LiveDocs Flavours
[![lite-badge](https://img.shields.io/badge/CRC1456-Jupyterlite-yellow)](https://crc1456.pages.gwdg.de/livedocs/b02-gibbsian-polar-slice-sampling)
[![static-html](https://img.shields.io/badge/CRC1456-axial_modes_dim_dep-white)](https://crc1456.pages.gwdg.de/livedocs/b02-gibbsian-polar-slice-sampling/files/axial_modes_dim_dep.html)
[![static-html](https://img.shields.io/badge/CRC1456-hyperplane_disk-white)](https://crc1456.pages.gwdg.de/livedocs/b02-gibbsian-polar-slice-sampling/files/hyperplane_disk.html)
[![static-html](https://img.shields.io/badge/CRC1456-logistic_regression-white)](https://crc1456.pages.gwdg.de/livedocs/b02-gibbsian-polar-slice-sampling/files/logistic_regression.html)

[![static-html](https://img.shields.io/badge/CRC1456-multivariate_std_Cauchy-white)](https://crc1456.pages.gwdg.de/livedocs/b02-gibbsian-polar-slice-sampling/files/multivariate_std_Cauchy.html)
[![static-html](https://img.shields.io/badge/CRC1456-Neal's_funnel-white)](https://crc1456.pages.gwdg.de/livedocs/b02-gibbsian-polar-slice-sampling/files/Neal's_funnel.html)
[![Docker](https://img.shields.io/badge/CRC1456-Dockerhub-blue)](https://gitlab.gwdg.de/crc1456/livedocs/b02-gibbsian-polar-slice-sampling/container_registry/2516)



## Overview

This repository contains the experiments and plots presented in the paper

Philip Schär, Michael Habeck, Daniel Rudolf  
Gibbsian Polar Slice Sampling  
Proceedings of the 40th International Conference on Machine Learning,  
PMLR 202:30204-30223, 2023  

Each experiment is contained in a separate Jupyter notebook. These notebooks
contain not only the source code specific to each experiment but also a
definition of the target distribution under consideration, as well as some
explanations regarding the target's properties, the overall purpose of the
experiment and the means by which we analyze the samplers' performances. For
context regarding the samplers themselves, we refer to the paper, available
on [arXiv](https://arxiv.org/pdf/2302.03945.pdf) and in the [conference
proceedings](https://proceedings.mlr.press/v202/schar23a/schar23a.pdf).

